package org.supercall.domainModel.framework;

/**
 * Created by kira on 16/8/3.
 */
public class FormConfig {
    private String displayName;
    private String key;
    private String inputType;
    private String pluginName;
    private String pluginFunction;
    private Boolean isRequire;
    private String validateType;

    public FormConfig(String displayName, String key, String inputType, String pluginName, String pluginFunction, Boolean isRequire, String validateType) {
        this.displayName = displayName;
        this.key = key;
        this.inputType = inputType;
        this.pluginName = pluginName;
        this.pluginFunction = pluginFunction;
        this.isRequire = isRequire;
        this.validateType = validateType;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getPluginName() {
        return pluginName;
    }

    public void setPluginName(String pluginName) {
        this.pluginName = pluginName;
    }

    public String getPluginFunction() {
        return pluginFunction;
    }

    public void setPluginFunction(String pluginFunction) {
        this.pluginFunction = pluginFunction;
    }

    public Boolean getRequire() {
        return isRequire;
    }

    public void setRequire(Boolean require) {
        isRequire = require;
    }

    public String getValidateType() {
        return validateType;
    }

    public void setValidateType(String validateType) {
        this.validateType = validateType;
    }

    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }
}
