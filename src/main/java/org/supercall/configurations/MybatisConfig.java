package org.supercall.configurations;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

//MyBatis配置器
@Configuration
@ImportResource({"classpath:/config/spring-mybatis.xml"})
public class MybatisConfig {
}